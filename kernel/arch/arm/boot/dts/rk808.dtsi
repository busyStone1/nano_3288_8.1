

&i2c0 {
	clock-frequency = <400000>;
	status = "okay";

	rk808: pmic@1b {
		compatible = "rockchip,rk808";
		reg = <0x1b>;
		interrupt-parent = <&gpio0>;
		interrupts = <4 IRQ_TYPE_LEVEL_LOW>;
		pinctrl-names = "default";
		pinctrl-0 = <&pmic_int &global_pwroff>;
		rockchip,system-power-controller;
		wakeup-source;
		#clock-cells = <1>;
//		clock-output-names = "rk808-clkout1", "rk808-clkout2";
		clock-output-names = "xin32k", "rk808-clkout2";

		vcc1-supply = <&vcc_sys>;
		vcc2-supply = <&vcc_sys>;
		vcc3-supply = <&vcc_sys>;
		vcc4-supply = <&vcc_sys>;
		vcc6-supply = <&vcc_sys>;
		vcc8-supply = <&vcc_io>;
		vcc9-supply = <&vcc_io>;
		vcc12-supply = <&vcc_io>;
		vddio-supply = <&vcc_io>;

		regulators {
			vdd_cpu: DCDC_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <1400000>;
				regulator-name = "vdd_arm";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd_gpu: DCDC_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <850000>;
				regulator-max-microvolt = <1250000>;
				regulator-name = "vdd_gpu";
				regulator-ramp-delay = <6000>;
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_ddr: DCDC_REG3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc_ddr";
				regulator-state-mem {
					regulator-on-in-suspend;
				};
			};

			vcc_io: DCDC_REG4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-name = "vcc_io";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3300000>;
				};
			};

			vcc_tp: LDO_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-name = "vcc_tp";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcca_codec: LDO_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-name = "vcca_codec";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3300000>;
				};
			};

			vdd_10: LDO_REG3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1000000>;
				regulator-name = "vdd_10";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1000000>;
				};
			};

			vcc_wl: LDO_REG4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc_wl";
				regulator-state-mem {
					regulator-on-in-suspend;
				};
			};

			vccio_sd: LDO_REG5 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-name = "vccio_sd";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd10_lcd: LDO_REG6 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1000000>;
				regulator-name = "vdd10_lcd";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_18: LDO_REG7 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc_18";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			vcc18_lcd: LDO_REG8 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc18_lcd";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_sd: SWITCH_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-name = "vcc_sd";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_lcd: SWITCH_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-name = "vcc_lcd";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};
		};
	};
	rtc {
                status = "okay";
        };
};


&pinctrl {
        pmic {
                pmic_int: pmic-int {
                        rockchip,pins = <RK_GPIO0 4 RK_FUNC_GPIO &pcfg_pull_up>;
                };
        };
};


&io_domains {
	status = "okay";
	audio-supply = <&vcc_18>;
	bb-supply = <&vcc_io>;
	dvp-supply = <&vcc18_dvp>;
	flash0-supply = <&vcc_flash>;
	flash1-supply = <&vcc_io>;
	gpio30-supply = <&vcc_io>;
	gpio1830 = <&vcc_wl>;
	lcdc-supply = <&vcc_lcd>;
	sdcard-supply = <&vccio_sd>;
	wifi-supply = <&vcc_wl>;
};
